﻿/++
 + Contains code to load in INI files.
 + ++/
module dassests.ini;

private
{
    import std.ascii        : isAlphaNum, isWhite;
    import std.uni          : toLower;
    import std.string       : strip, startsWith, endsWith;
    import std.algorithm    : all, filter, each;

    import std.exception, std.range;
}

/++
 + Represents an property in an Ini file.
 + ++/
struct IniProperty
{
    /// The section the property belongs to. Lowercase
    string section;

    /// The name of the property. Lowercase
    string name;

    /// The value of the property
    string value;
}

/++
 + Contains data on an Ini file.
 + 
 + Supports (most of these may seem like they're there because I'm lazy. They're not, I want to allow most of these for one reason or another):
 +  - Comments don't have to be on their own lines
 +  - '#' as a comment character.
 +  - Properties without a section. (Their "section" field will simply be an empty string.)
 +  - Section names can be reused. The properties of previous declarations of that section stay in-tact. (Not sure if I should allow this, but I don't see why not)
 +  - Names are case-insensitive, and are stored in lower-case.
 +  - Empty lines are allowed.
 +  - Property and section names are stripped at the start and end for whitespace.
 +  - Properties with the same name may belong to the same section.
 +  - The leading and trailing spaces in a value are stipped off. Wrap the value inside quote marks to avoid this.
 +  - Supports using a ':' in place of an '='.
 + ++/
@safe
struct Ini
{
    private
    {
        string  _text;
        size_t  _index;
        size_t  _line = 1;

        // I'm planning to use an IAllocator in the future for memory allocations.
        // So I want to stay away from using the built-in associative arrays.
        // It's slower this way(probably), however.
        IniProperty[] _properties;
    }

    public
    {
        /++
         + Parses the given text as an INI file.
         + 
         + Parameters:
         +  iniData     = The INI file's text.
         + ++/
        this(string iniData)
        {
            this._text  = iniData;
            this.parseData();
        }

        /++
         + Creates and returns a filter range that will filter the properties by their section name.
         + 
         + Parameters:
         +  section = The name of the section to get the properties of. Case-insensitive
         + 
         + Returns:
         +  A range that filters out any properties that don't belong to $(B section)
         + ++/
        auto propertiesBySection(string section)
        {
            auto lower = section.toLower();
            return this.properties.filter!((prop) => prop.section == lower);
        }
        unittest
        {
            Ini ini = Ini(r"
                [Test]
                Prop1=Daniel
                Prop2=Is

                [Test2]
                Ignore=Not

                [Test] ; Duplicate sections are allowed.
                Prop3=Soupy
            ");

            foreach(property; ini.propertiesBySection("Test"))
            {
                assert(property.section == "test");
                assert(property.name    != "ignore");
                assert(property.value   != "Not");
            }
        }

        /++
         + Attempts to get a value of a certain property, returning $(B _default) if the propety cannot be found.
         + If multiple matching properties are found, then only the first found one is used.
         + 
         + Parameters:
         +  section = The section the property belongs to. Case-insensitive
         +  name = The name of the property. Case-insensitive
         +  _default = The default value to return if no property is found.
         + 
         + Returns:
         +  Either the value of the wanted property, or $(B _default)
         + ++/
        string get(string section, string name, string _default = "") const
        {
            section = section.toLower();
            name    = name.toLower();

            auto range = this.properties.filter!((prop) => prop.section == section && prop.name == name);

            return (range.empty) ? _default : range.front.value;
        }
        unittest
        {
            Ini ini = Ini(r"
                [Test]
                Prop1=Daniel
                Prop2=Is

                [Test2]
                Ignore=Not

                [Test] ; Duplicate sections are allowed.
                Prop1=Soupy
            ");

            assert(ini.get("Test",  "Prop1")  == "Daniel");
            assert(ini.get("tEsT2", "iGnOrE") == "Not");
            assert(ini.get("Test",  "Ignore", "Error") == "Error");
            assert(ini.get("Hello", "", "Also an Error") == "Also an Error");
        }

        /++
         + Determines if any properties that are under $(B section) and are called $(B name) are found.
         + 
         + Parameters;
         +  section = The section the property belongs to. Case-insensitive
         +  name = The name of the section. Case-insensitive
         + 
         + Returns:
         +  True if any matching properties are found. False otherwise.
         + ++/
        bool exists(string section, string name) const
        {
            return (this.get(section, name, null) !is null);
        }
        unittest
        {
            auto ini = const Ini(r"
                [Test]
                Prop1=Daniel
                Prop2=Is

                [Test2]
                Ignore=Not
            ");

            assert(ini.exists("Test", "Prop1"));
            assert(ini.exists("Test2", "Ignore"));
            assert(!ini.exists("Hello", "World!"));
        }

        /++
         + Get every property found in the Ini file. Stored in the order they're found in the Ini file itself
         + ++/
        @property @nogc
        const(IniProperty)[] properties() pure nothrow const
        {
            return this._properties;
        }

        /++
         + Creates an Ini string from this struct.
         + ++/
        string toString()
        {
            auto append = appender!string;
            append.reserve(16 * this.properties.length); // Let's just assume that on average, a property will add around 16 bytes to the appender

            string section = "";
            foreach(prop; this.properties) with(append)
            {
                if(section != prop.section)
                {
                    section = prop.section;
                    put("\n[");
                    put(prop.section);
                    put("]\n");
                }
                put(prop.name);
                put("=");

                if(prop.value.startsWith(" ") || prop.value.endsWith(" "))
                {
                    put("'");
                    put(prop.value);
                    put("'");
                }
                else
                {
                    put(prop.value);
                }

                put("\n");
            }

            return append.data;
        }
        unittest
        {
            Ini ini = Ini(r"
                [Test]
                Prop1=Daniel
                Prop2: ' Is '

                [Test2]
                Ignore=Not
            ");

            assert(ini.toString() ==
                "\n[test]\nprop1=Daniel\nprop2=' Is '\n\n[test2]\nignore=Not\n", ini.toString());
        }
    }

    private
    {
        // Doesn't call "ParseData", just used for unittests.
        this(string iniData, bool ignore)
        {
            this._text  = iniData;
        }

        /++
         + Determines if we're at the end of the text.
         + ++/
        @property @nogc
        bool eof() pure nothrow
        {
            return (this._index >= this._text.length);
        }
        unittest
        {
            Ini ini = Ini(" ", false);

            assert(!ini.eof);
            ini._index += 1;
            assert(ini.eof);
        }

        /++
         + Skips over whitespace. 
         + ++/
        void skipWhitespace()
        {
            while(!this.eof)
            {
                auto next = this.nextChar;
                if(!isWhite(next))
                {
                    this._index -= 1;
                    break;
                }
            }
        }
        unittest
        {
            Ini ini = Ini("a \n\tb", false);
            ini.skipWhitespace();

            assert(ini._index == 0);
            ini._index += 1;
            ini.skipWhitespace();

            assert(ini._index == 4);
        }

        /++
         + Returns the next character in the text.
         + Assumes that a character is expected, so throws on eof.
         + ++/
        char nextChar()
        {
            enforce(!this.eof, new IniException("Unexpected EoF", this._line));

            auto next = this._text[this._index++];
            if(next == '\n')
            {
                this._line += 1;
            }

            return next;
        }

        /++
         + Skips over the line.
         + If a comment character isn't found, and a non-whitespace character is found, then an exception is thrown.
         + ++/
        void skipLine()
        {
            bool foundComment = false;
            while(!this.eof)
            {
                auto next = this.nextChar;

                if(next == '\n')
                {
                    break;
                }
                else if(this.isComment(next))
                {
                    foundComment = true;
                }

                enforce(isWhite(next) || foundComment, new IniException("Unexpected character '"~next~"'", this._line));
            }
        }
        unittest
        {
            Ini ini = Ini(" \nb", false);
            ini.skipLine();

            import std.file : text;
            assert(ini._index == 2, text(ini._index));
            assertThrown!IniException(ini.skipLine);
        }

        /++
         + Determines if the character given is a comment character.
         + ++/
        @nogc
        bool isComment(char comment) pure nothrow
        {
            return (comment == ';' || comment == '#');
        }
        unittest
        {
            Ini ini = Ini("#;b", false);

            assert(ini.isComment(ini.nextChar));
            assert(ini.isComment(ini.nextChar));
            assert(!ini.isComment(ini.nextChar));

            assertThrown!IniException(ini.nextChar);
        }

        /++
         + Parses the ini file
         + ++/
        void parseData()
        {
            this.skipWhitespace();

            string currentSection = "";

            // Keep parsing data until there's no more text left.
            while(!this.eof)
            {
                auto next = this.nextChar;

                // If a comment is read in, then skip over the line, which it has some logic to handle it.
                if(this.isComment(next))
                {
                    // We go back by 1 so skipLine reads in the comment character.
                    this._index -= 1;
                    this.skipLine();
                }
                else if(next == '[') // Read in a section
                {
                    // Find where the next ']' is, throwing on \n or eof.
                    size_t start = this._index;
                    while(true)
                    {
                        next = this.nextChar;

                        if(next == ']')
                        {
                            break;
                        }

                        enforce(next != '\n', new IniException("Unterminated section name. The ']' must be on the same line.", this._line));
                    }

                    // Get the name
                    currentSection = this._text[start..this._index-1].toLower().strip();
                    enforce(currentSection.length != 0, new IniException("Section names must be at least 1 character long.", this._line));

                    this.skipLine();
                }
                else // Assume it's a property by this point
                {
                    // Property names keep all of the whitespace.
                    // The entire property must be kept on a single line.
                    auto property = IniProperty(currentSection);

                    // Read in the property name
                    size_t start = (this._index - 1);
                    while(true)
                    {
                        next = this.nextChar;

                        if(next == '=' || next == ':')
                        {
                            break;
                        }

                        enforce(next != '\n', new IniException("Properties must be declared on a single line. (Or there's an incomplete property)", this._line));
                    }

                    property.name = this._text[start..this._index-1].toLower().strip();
                    enforce(property.name.length > 1, new IniException("Property names must be at least 1 character long.", this._line));

                    // Read in the value
                    start = this._index;
                    bool useQuotes = false;
                    bool canUseQuotes = true;
                    while(true)
                    {
                        next = this.nextChar;

                        // If the value is using quotes
                        if(next == '\n' || this.isComment(next) || this.eof || (useQuotes && next == '\''))
                        {
                            enforce(!useQuotes || next == '\'', new IniException("Unterminated value. Expected a terminating \"'\".", this._line));

                            break;
                        }

                        // If we find a quote mark, move the start to the quote and then stop reading once another quote is found.
                        // We must also make sure that only whitespace has been read in, otherwise we're actually skipping data.
                        // If non-whitespace has been read in, then just consider the "'" as part of the value.
                        else if(next == '\'' && canUseQuotes)
                        {
                            useQuotes = true;
                            start = this._index;
                        }
                        else if(!isWhite(next))
                        {
                            canUseQuotes = false;
                        }
                    }

                    // If we've hit eof, then "index-1" would cut off the very last character in the value, I hope to avoid this.
                    property.value = this._text[start..this.eof ? this._index : this._index-1];

                    // If we didn't use quotes, stip off any spaces
                    if(!useQuotes)
                    {
                        property.value = property.value.strip();
                    }

                    if(this.isComment(next))
                    {
                        this._index -= 1;
                        this.skipLine;
                    }

                    this._properties ~= property;
                }

                this.skipWhitespace();
            }
        }
        unittest
        {
            Ini ini = Ini(
            r"
                [Test]
                Test One=This is a test
                Test Two=5030
                Test One = This is a duplicate! Still allowed though. ; Note that the spaces at the start and end of the value are stripped off.
                Test Three: ' This has spaces retained ' ; Quotes can be used to retain spaces
                Test Four: O'Reily ; If the value doesn't start with a quote, then it's seen as a normal value, so any further quotes hold no extra meaning.
            ");

            import std.file : text;
            assert(ini.properties == 
                [
                    IniProperty("test", "test one", "This is a test"),
                    IniProperty("test", "test two", "5030"),
                    IniProperty("test", "test one", "This is a duplicate! Still allowed though."),
                    IniProperty("test", "test three", " This has spaces retained "),
                    IniProperty("test", "test four", "O'Reily")
                ], text(ini.properties));
        }
    }
}
@safe
unittest
{
    Ini ini = Ini(r"
; Random ini file given to me by a friend.

; Use / (forward slash) in path names
;
[game]
name = 'Evard Test GamePack'
versionGlobal = 0
versionLocal = 0
versionPatch = 0
gCode = EVRD
AccountsFolder = ./accounts
LogsFolder = ./logs
DataFolder = ./data
Database = evard.db

[npcclient]
Logon = npcclient
Database = evardnpc.db

[universe]
Attributes = 4
Races = 3
Species = 3

[attribute1]
Description = physical strength
attribId = strng
player = yes
creationMin = 3
creationMax = 18
floorValue = 1
ceilingValue = 25

[attribute2]
Description = intelligence
attribId = intel
player = yes
creationMin = 3
creationMax = 18
floorValue = 1
ceilingValue = 25

[attribute3]
Description = charisma
attribId = chsma
player = yes
creationMin = 3
creationMax = 18
floorValue = 1
ceilingValue = 25

[attribute4]
Description = bite with teeth
attribId = tbite
player = no
floorValue = 1
ceilingValue = 25

[race1]
Name = human

[race2]
Name = dwarf
attributes = strng,2,chsma,-1

[race3]
Name = elf
attributes = intel,1,chsma,2

[species1]
Name = wolf
attributes = tbite,0

[species2]
Name = bear
attributes = tbite,2

[species3]
Name = giant rat
attributes = tbite,-1

[maps]
Format = SLTM
VersionMajor = 0
VersionMinor = 0
Name = evard
Number = 2
StartMap = 1
StartX = 1
StartY = 11");

    // If it's made it this far, then I declare it to be usable for my own personal interests.
    // But just to be safe, I'll do an assert for "maps"
    import std.range : array;
    import std.file  : text;
    auto values = ini.propertiesBySection("maps").array;

    assert(values == [
            IniProperty("maps", "format",       "SLTM"),
            IniProperty("maps", "versionmajor", "0"),
            IniProperty("maps", "versionminor", "0"),
            IniProperty("maps", "name",         "evard"),
            IniProperty("maps", "number",       "2"),
            IniProperty("maps", "startmap",     "1"),
            IniProperty("maps", "startx",       "1"),
            IniProperty("maps", "starty",       "11")
        ], text(values));

    assert(ini.get("Game", "Database", "[Not Found]") != "[Not Found]");
}

/++
 + Thrown when something goes wrong with reading in an INI file.
 + ++/
class IniException : Exception
{
    @safe pure nothrow this(string msg, size_t debugLine, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        import std.file : text;
        super(text("On line ", debugLine, ": ", msg), file, line, next);
    }
}